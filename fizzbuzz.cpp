#include <fizzbuzz.hpp>

#include <sstream>

namespace {
  const std::string FIZZ("Fizz");
  const std::string BUZZ("Buzz");

bool isAMultipleOf3(uint32_t number)
{
  return ((number % 3) == 0);
}

bool isAMultipleOf5(uint32_t number)
{
  return ((number % 5) == 0);
}

};

const std::string FizzBuzz::display(uint32_t number) const
{
  std::stringstream toDisplay;

  if( isAMultipleOf3(number)) {
    toDisplay << FIZZ;
  }

  if ( isAMultipleOf5(number)) {
    toDisplay << BUZZ;
  }

  if ( toDisplay.str().empty() ) {
    toDisplay << std::to_string(number);
  }

  return toDisplay.str();
}
