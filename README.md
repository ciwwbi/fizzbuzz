# fizzbuzz

This project is one realisation of kata fizzbuzz.

## Statement

Implement theses functionnalities as they are enounced :
* an arbitrary number is displayed as is
* if number is divisible by 3, display Fizz
* if number is divisible by 5, display Buzz
* if number is divisible by 3 and 5, display FizzBuzz

## How to compile

Tests use boost library in version 1.67. It should be installed in ../resources/boost_1_67_0
You're able to modify Makefile (BOOST variable) if your boost library is elsewhere or in another version.

It has been compiled with gcc 11.2.0 and GNU Make 4.3

```
$ gmake test
./testfizzbuzz
Running 8 test cases...

*** No errors detected
```
