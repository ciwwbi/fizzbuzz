#ifndef FIZZBUZZ_HPP
#define FIZZBUZZ_HPP

#include <string>
#include <cstdint>

class FizzBuzz
{
  public:
    const std::string display(uint32_t number) const;
};

#endif
