BOOST=../resources/boost_1_67_0

CC=g++
CFLAGS=-Wall -O  -isystem$(BOOST) -I.
LDFLAGS=
EXEC=testfizzbuzz
SRC=$(wildcard *.cpp)
OBJ=$(SRC:.cpp=.o)

all: $(EXEC)

test: $(EXEC)
	./$(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -f *.o core $(EXEC)
