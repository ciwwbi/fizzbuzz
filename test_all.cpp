#define BOOST_TEST_MODULE FizzBuzz
#include <boost/test/included/unit_test.hpp>

#include <fizzbuzz.hpp>


BOOST_AUTO_TEST_CASE(display_1_when_1)
{
  FizzBuzz fizzbuzz;

  std::string effective = fizzbuzz.display(1);

  std::string expected = "1";
  BOOST_REQUIRE_EQUAL(expected, effective);
}

BOOST_AUTO_TEST_CASE(display_n_when_n)
{
  FizzBuzz fizzbuzz;

  std::string effective = fizzbuzz.display(7);

  std::string expected = "7";
  BOOST_REQUIRE_EQUAL(expected, effective);
}

BOOST_AUTO_TEST_CASE(display_Fizz_when_3)
{
  FizzBuzz fizzbuzz;

  std::string effective = fizzbuzz.display(3);

  std::string expected = "Fizz";
  BOOST_REQUIRE_EQUAL(expected, effective);
}

BOOST_AUTO_TEST_CASE(display_Fizz_when_multipleOf3)
{
  FizzBuzz fizzbuzz;

  std::string effective = fizzbuzz.display(9);

  std::string expected = "Fizz";
  BOOST_REQUIRE_EQUAL(expected, effective);
}

BOOST_AUTO_TEST_CASE(display_Buzz_when_5)
{
  FizzBuzz fizzbuzz;

  std::string effective = fizzbuzz.display(5);

  std::string expected = "Buzz";
  BOOST_REQUIRE_EQUAL(expected, effective);
}

BOOST_AUTO_TEST_CASE(display_Buzz_when_multipleOf5)
{
  FizzBuzz fizzbuzz;

  std::string effective = fizzbuzz.display(20);

  std::string expected = "Buzz";
  BOOST_REQUIRE_EQUAL(expected, effective);
}

BOOST_AUTO_TEST_CASE(display_FizzBuzz_when_15)
{
  FizzBuzz fizzbuzz;

  std::string effective = fizzbuzz.display(15);

  std::string expected = "FizzBuzz";
  BOOST_REQUIRE_EQUAL(expected, effective);
}

BOOST_AUTO_TEST_CASE(display_FizzBuzz_when_multipleOf15)
{
  FizzBuzz fizzbuzz;

  std::string effective = fizzbuzz.display(45);

  std::string expected = "FizzBuzz";
  BOOST_REQUIRE_EQUAL(expected, effective);
}

